﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Pruebas.Comunes.Contenedores
{

	[Serializable()]
	[KnownType(typeof(EscenarioBase))]
	public abstract class EscenarioBase : IEscenario
	{

		public abstract string Resultado { get; set; }

		public abstract string MensajeErrorRecibido { get; set; }
		public abstract string MensajeErrorEsperado { get; set; }
	}
}
