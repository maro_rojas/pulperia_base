﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Pruebas.Comunes.Extensiones
{
	public class ObtenerEscenarios
	{
		/// <summary>
		/// Serializa en una lista T los escenarios contenidos en el archivo de insumo
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="archivoInsumo">Archivo de insumo, debe ser un excel version 97-2003</param>
		/// <param name="inicializarValoresPorDefecto">Indica si se deben inicializar los valores por defecto de las propiedades</param>
		/// <returns>Una lista de tipo T con los objetos contenidos por el excel</returns>
		public List<T> ObtenerListaEscenariosDesdeArchivo<T>(byte[] archivoInsumo, bool inicializarValoresPorDefecto = true) where T : class
		{
			var listaEscenarios = new List<T>();
			var filePath = Path.GetTempFileName();
			try
			{
				File.WriteAllBytes(filePath, archivoInsumo);

				string con = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}; Extended Properties='Excel 8.0;HDR=Yes;'", filePath);
				using (OleDbConnection connection = new OleDbConnection(con))
				{
					connection.Open();
					OleDbCommand command = new OleDbCommand("select * from [Hoja1$]", connection);
					using (OleDbDataReader dr = command.ExecuteReader())
					{
						listaEscenarios = dr.ToList<T>(inicializarValoresPorDefecto);
					}
				}
			}
			finally
			{
				File.Delete(filePath);
			}

			return listaEscenarios;
		}
	}
}
