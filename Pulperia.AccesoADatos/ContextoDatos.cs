namespace Pulperia.AccesoADatos
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;
	using Pulperia.Modelos;
	using System.Data.Entity.ModelConfiguration.Conventions;

	public partial class ContextoDatos : DbContext
	{
		public ContextoDatos()
			: base("name=ContextoDatos")
		{
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			this.Configuration.LazyLoadingEnabled = false;
			this.Configuration.ProxyCreationEnabled = false;
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
