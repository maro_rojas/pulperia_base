﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;

/// <summary> 
/// Mapper,
/// The class can optionally accept a custom action function, (aka delegate).
/// If a custom action is supplied, it will be run after the automatic synchronization.
/// That way, the action will override the automatic synchronization behavior for 
/// any fields that it changes.
/// http://stackoverflow.com/questions/286294/object-to-object-mapper
/// </summary>
public class Mapper
{
	/// <summary> 
	/// MapObjects,
	/// Call this function to map data between two objects.
	/// See the class description and the examples function for additional details.
	/// </summary>
	public static D MapObjects<S, D>(S Source, Action<S, D> OptionalCustomAction = null)
		where S : class
		where D : class
	{
		// Get the types of the supplied objects.
		var SType = typeof(S);
		var DType = typeof(D);

		var Destination = Activator.CreateInstance(DType);

		// Get the properties and fields of the supplied objects.
		PropertyInfo[] SProperties = SType.GetProperties();
		PropertyInfo[] DProperties = DType.GetProperties();

		// Perform automatic synchronization of the objects.
		SyncObjects(Source, Destination, SProperties, DProperties);

		// Apply the custom action function, if one was supplied.
		if (OptionalCustomAction != null) { OptionalCustomAction(Source, (D)Destination); }

		return (D)Destination;
	}

	/// <summary> 
	/// SyncObjects,
	/// This is a private helper function.
	/// Perform the automatic copy.
	/// </summary>
	private static void SyncObjects<S, D>(S Source, D Destination, PropertyInfo[] SProperties, PropertyInfo[] DProperties)
	{
		foreach (var FromProperty in SProperties)
		{
			// Match properties to properties.
			var destinationProperty = DProperties.FirstOrDefault(x => x.Name == FromProperty.Name);
			if (destinationProperty != null)
			{
				if (FromProperty.PropertyType.Name == destinationProperty.PropertyType.Name)
				{
					var value = FromProperty.GetValue(Source, null);
					destinationProperty.SetValue(Destination, value, null);
					continue;
				}
			}
		}
	}
}