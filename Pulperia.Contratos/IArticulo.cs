﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Pulperia.Modelos;
using Pulperia.Modelos.Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Web;

namespace Pulperia.Contratos
{
	[ExceptionShielding]
	[ServiceContract]
	public interface IArticulo
	{
		[OperationContract]
		bool ServicioDisponible();

		[OperationContract]
		List<Articulo> ListarArticulos();

		[OperationContract]
		[WebGet(UriTemplate = "Articulos", ResponseFormat = WebMessageFormat.Json)]
		List<Articulo> ListarDesdeBaseDatos();

		[OperationContract]
		void InactivarArticulo(Pulperia.Modelos.Articulo nuevoArticulo);

		[OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ObtenerArticulos/{CodArticulo}", ResponseFormat = WebMessageFormat.Json)]
        Articulo ObtenerPorCodArticulo(string CodArticulo);

		[OperationContract]
		[FaultContract(typeof(PulperiaFault))]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Bare,
           UriTemplate = "InsertarArticulo")]
		void InsertarArticulo(Modelos.Articulo nuevoArticulo);

        [OperationContract]
        [FaultContract(typeof(PulperiaFault))]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Bare,
           UriTemplate = "ActualizarArticulo")]
        void ActualizarArticulo(Modelos.Articulo nuevoArticulo);

		[OperationContract]
		[FaultContract(typeof(PulperiaFault))]
		void LanzarExcepcion();
    }
}
