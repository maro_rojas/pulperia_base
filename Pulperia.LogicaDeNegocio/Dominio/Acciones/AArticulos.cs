﻿using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.LogicaDeNegocio.Dominio.Acciones
{
    public class AArticulos
    {
        public void InactivarArticulo(Articulo nuevoArticulo)
        {
            using (var rep = new Pulperia.AccesoADatos.Repositorios.Articulos())
            {
                Articulo a = rep.Articulo.First(i => i.IdArticulo == nuevoArticulo.IdArticulo);
                a.CodEstado = (int)Modelos.Enumerados.Estados.Inactivo;
                rep.SaveChanges();
            }
        }

        public void GuardarArticulo(Articulo nuevoArticulo)
        {
            using (var rep = new Pulperia.AccesoADatos.Repositorios.Articulos())
            {
                if (nuevoArticulo.CodEstado == (int)Modelos.Enumerados.Estados.Inactivo)
                {
                    nuevoArticulo.CodEstado = (int)Modelos.Enumerados.Estados.Activo;
                }
                rep.Articulo.Add(nuevoArticulo);
                rep.SaveChanges();
            }
        }

        public Articulo ObtenerArticuloPorCodigoArticulo(Articulo articulo)
        {
            using (var rep = new Pulperia.AccesoADatos.Repositorios.Articulos())
            {
                return rep.Articulo.FirstOrDefault(x => x.CodArticulo == articulo.CodArticulo);
            }
        }

        public void ActualizarArticulo(Articulo articulo)
        {
            using (var repositorio = new Pulperia.AccesoADatos.Repositorios.Articulos())
            {
                Articulo a = repositorio.Articulo.First(i => i.CodArticulo == articulo.CodArticulo);
                a.Nombre = articulo.Nombre;
                a.Precio = articulo.Precio;
                a.Existencia = articulo.Existencia;
                a.CodEstado = articulo.CodEstado;
                repositorio.SaveChanges();
            }
        }
    }
}
