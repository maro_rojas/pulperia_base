﻿using Pulperia.LogicaDeNegocio.Dominio.Acciones;
using Pulperia.LogicaDeNegocio.Dominio.Validaciones;
using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Pulperia.LogicaDeNegocio.Dominio.Caracteristicas
{
    public class CaracteristicaAgregarArticulo : CaracteristicaBase<Articulo>
    {

        public override bool Validar(Articulo item)
        {
            var validador = new VArticulo();
            var resultado = validador.LaExistenciaNoEsMayorACero(item) &&
                            validador.ElPrecioNoEsValido(item) &&
                            validador.ElNombreNoDebeEstarVacio(item);

            if (!resultado)
                this.Excepciones.Add(validador.Excepcion);

            return resultado;
        }


        private bool ValidarSiElArticuloNoExiste(Articulo articuloEncontrado)
        {
            var validador = new VArticulo();
            var resultado = validador.ElArticuloNoExiste(articuloEncontrado);

            if (!resultado)
                this.Excepciones.Add(validador.Excepcion);

            return resultado;
        }


        public void Agregar(Articulo item)
        {
            var accion = new Pulperia.LogicaDeNegocio.Dominio.Acciones.AArticulos();
            if (!Validar(item))
                throw this.Excepciones.FirstOrDefault();

            //TODO: Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar

            var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

            //Existe en la BD
            if (!ValidarSiElArticuloNoExiste(articuloEncontrado))
                throw this.Excepciones.FirstOrDefault();
            else
            {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    //TODO: Aquí el código para guardar utilizando una accion
                    accion.GuardarArticulo(item);
                    scope.Complete();
                }
            }                
        }

        public void Inactivar(Articulo item)
        {
            var accion = new AArticulos();

            //TODO: Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar
            var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

            //Existe en la BD
            if (articuloEncontrado != null)
            {
                if (articuloEncontrado.CodEstado == (int)Modelos.Enumerados.Estados.Activo)
                {
                    //ArticuloEncontradoPorCodArticulo.CodEstado = 2;
                    accion.InactivarArticulo(articuloEncontrado);
                }
                else
                    throw this.Excepciones.FirstOrDefault();
            }
            else
                throw this.Excepciones.FirstOrDefault();
        }

        public void Actualizar(Articulo item)
        {
            var accion = new AArticulos();

            if (!Validar(item))
            {
                throw this.Excepciones.FirstOrDefault();
            }

            //TODO:Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar
            var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

            //Existe en la BD
            if (articuloEncontrado!=null)
            {
                accion.ActualizarArticulo(item);
            }
            else // No Existe en la BD
            {
                throw this.Excepciones.FirstOrDefault();
            }                                     
        }

    }
}
