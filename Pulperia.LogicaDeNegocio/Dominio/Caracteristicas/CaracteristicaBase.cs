﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Threading;
using System.Collections.Concurrent;


public abstract class CaracteristicaBase<TModelo> : IDisposable
{

	public CaracteristicaBase()
	{
		this.contextoOperacion = OperationContext.Current;
		this.listaExcepciones = new BlockingCollection<Exception>();
		this.contextoHttp = HttpContext.Current;
		this.mPrincipal = Thread.CurrentPrincipal;
	}

	protected OperationContext contextoOperacion;
	protected HttpContext contextoHttp;
	protected BlockingCollection<Exception> listaExcepciones;

	private System.Security.Principal.IPrincipal mPrincipal;
	public BlockingCollection<Exception> Excepciones
	{
		get { return listaExcepciones; }
	}

	public string ExcepcionesFormateadas
	{
		get {
			dynamic lista = (from e in Excepciones select e.Message);
			return string.Join(", " + Environment.NewLine, lista);
		}
	}

	#region "Metodos"
	public void InicializarContextoOperacion()
	{
		if (this.contextoOperacion != null)
		{
			OperationContext.Current = this.contextoOperacion;
		}

		if (this.contextoHttp != null)
		{
			HttpContext.Current = this.contextoHttp;
		}
		Thread.CurrentPrincipal = this.mPrincipal;
	}

	public abstract bool Validar(TModelo item);
	#endregion

	#region "IDisposable Support"
	private bool disposedValue;
	protected virtual void Dispose(bool disposing)
	{
		if (!this.disposedValue)
		{
			if (disposing)
			{
				this.listaExcepciones.Dispose();
			}
			this.listaExcepciones = null;
		}
		this.disposedValue = true;
	}
	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}
	#endregion

}