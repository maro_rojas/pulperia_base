﻿using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.LogicaDeNegocio.Dominio.Validaciones
{
	public class VArticulo : ValidacionBase
	{
		//public bool ArticuloActivo(Articulo articuloValidar)
		//{
		//	var resultado = string.IsNullOrEmpty(articuloValidar.Nombre);
		//	if (!resultado)
		//	{
		//		this.Excepcion = new Pulperia.Modelos.Excepciones.PulperiaException(Pulperia.Modelos.Enumerados.CodigosError.ArticuloInvalido.Descripcion(), (int)Pulperia.Modelos.Enumerados.CodigosError.ArticuloInvalido);
		//	}

		//	return resultado;
		//}

		public bool ElNombreNoDebeEstarVacio(Articulo articuloValidar)
		{
			var resultado =!string.IsNullOrEmpty(articuloValidar.Nombre);
			if (!resultado)
			{
				this.Excepcion = new Pulperia.Modelos.Excepciones.PulperiaException(Pulperia.Modelos.Enumerados.CodigosError.NombreVacio.Descripcion(), (int)Pulperia.Modelos.Enumerados.CodigosError.NombreVacio);
			}

			return resultado;
		}

		public bool ElPrecioNoEsValido(Articulo articuloValidar)
		{
			var resultado = articuloValidar.Precio > 0;
			if (!resultado)
			{
				this.Excepcion = new Pulperia.Modelos.Excepciones.PulperiaException(Pulperia.Modelos.Enumerados.CodigosError.PrecioInvalido.Descripcion(), (int)Pulperia.Modelos.Enumerados.CodigosError.PrecioInvalido);
			}

			return resultado;
		}

		public bool LaExistenciaNoEsMayorACero(Articulo articuloValidar)
		{
			var resultado = articuloValidar.Existencia > 0;
			if (!resultado)
			{
				this.Excepcion = new Pulperia.Modelos.Excepciones.PulperiaException(Pulperia.Modelos.Enumerados.CodigosError.ExistenciaInvalida.Descripcion(), (int)Pulperia.Modelos.Enumerados.CodigosError.ExistenciaInvalida);
			}
			
			return resultado;
		}

		public bool ElArticuloNoExiste(Articulo articuloEncontrado)
		{
			var resultado = articuloEncontrado == null;
			if (!resultado)
			{
				this.Excepcion = new Pulperia.Modelos.Excepciones.PulperiaException(Pulperia.Modelos.Enumerados.CodigosError.ArticuloInvalido.Descripcion(), (int)Pulperia.Modelos.Enumerados.CodigosError.ArticuloInvalido);
			}

			return resultado;
		}
	}
}
