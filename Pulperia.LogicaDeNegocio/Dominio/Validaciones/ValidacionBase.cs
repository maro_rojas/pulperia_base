﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using Pulperia.Modelos;

public abstract class ValidacionBase
{

	public Exception Excepcion { get; set; }
	public List<ValidationResult> ListaResultadosValidacion { get; set; }

	public bool ValidaModelMedianteAnotaciones(ModeloBase model)
	{
		//debemos pasar la lista creada al método del validador
		this.ListaResultadosValidacion = new List<ValidationResult>();

		if (!Validator.TryValidateObject(model, new ValidationContext(model, null, null), this.ListaResultadosValidacion, true))
		{
			//debemos construir una excepción con la información de las validaciones que fallaron. La dejamos en la propiedad Excepcion nuestra.
			ConvertirValidacionesFallidasEnExcepcion(ListaResultadosValidacion);
			return false;
		}
		else
		{
			//limpiamos las variables del validador pues la validación dio verdadera
			this.ListaResultadosValidacion = null;
			this.Excepcion = null;
			return true;
		}
	}

	private void ConvertirValidacionesFallidasEnExcepcion(List<ValidationResult> listaValidaciones)
	{
		if (listaValidaciones != null)
		{
			if (listaValidaciones.Count > 0)
			{
				System.Text.StringBuilder textoExcepcion = new System.Text.StringBuilder();
				textoExcepcion.Append("La validación falló. Los siguientes errores fueron encontrados:");
				foreach (ValidationResult resValidacion in listaValidaciones)
				{
					textoExcepcion.Append(string.Format(" Propiedad: [{0}], Error: [{1}]", resValidacion.MemberNames.FirstOrDefault(), resValidacion.ErrorMessage));
				}
				this.Excepcion = new Exception(textoExcepcion.ToString());
			}
			else
				this.Excepcion = null;
		}
		else
			this.Excepcion = null;
	}

}