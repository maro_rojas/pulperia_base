namespace Pulperia.Modelos
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Runtime.Serialization;

	[DataContract]
	[Serializable]
	public partial class Articulo
	{
		[Key]
		[DataMember]
		public int IdArticulo { get; set; }
		[DataMember]
		public string CodArticulo { get; set; }
		[DataMember]
		public string Nombre { get; set; }
		[DataMember]
		public int Precio { get; set; }
		[DataMember]
		public int Existencia { get; set; }
		[DataMember]
		public int CodEstado { get; set; }

		public override string ToString()
		{
			return string.Format("CodArticulo: {0} - Articulo: {1} - Precio {2}", CodArticulo, Nombre, Precio);
		}
	}
}
