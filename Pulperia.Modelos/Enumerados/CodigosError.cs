﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Modelos.Enumerados
{

	[Serializable]
	[DataContract]
	public enum CodigosError : int
	{

        [EnumMember(Value = "Ninguno"), Description("Ninguno")]
        Ninguno = 0,
        [EnumMember(Value = "NombreVacio"), Description("Nombre no puede estar vacio")]
        NombreVacio = 1,
        [EnumMember(Value = "PrecioInvalido"), Description("Precio debe ser mayor a cero")]
        PrecioInvalido = 2,
        [EnumMember(Value = "ExistenciaInvalida"), Description("Existencia debe ser mayor a cero")]
        ExistenciaInvalida = 3,
        [EnumMember(Value = "ArticuloInvalido"), Description("El articulo ya está registrado en el sistema")]
        ArticuloInvalido = 4,
	}

}
