﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Pulperia.Modelos;

namespace Pulperia.Modelos.Excepciones
{

	[Serializable]
	public class PulperiaException : ExcepcionBase
	{

		#region "Constructores"

		public PulperiaException(string mensaje, int numError)
			: base(mensaje, numError, "", new object())
		{

		}

		public PulperiaException(string mensaje, IDictionary<string, string> datos, Modelos.Enumerados.CodigosError numError)
			: base(mensaje, null, datos: datos, acciones: new List<string>())
		{
			this.NumeroError = (int)numError;
		}

		public PulperiaException(string mensaje, Exception excepcionInterna, IDictionary<string, string> datos, Modelos.Enumerados.CodigosError numError)
			: base(mensaje, excepcionInterna, datos: datos, acciones: new List<string>())
		{
			this.NumeroError = (int)numError;
		}

		/// <summary>
		/// Constructor para la deserialización de la excepción mediante ISerializable
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		/// <remarks></remarks>
		protected PulperiaException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			//Aqui se deben sacar los datos específicos de esta excepción, por ejemplo:
			//Me.PropiedadX = info.GetValue("PropiedadX", GetType(String))
			//Me.PropiedadY = info.GetValue("PropiedadY", GetType(List(Of Integer)))
		}
		#endregion

		/// <summary>
		/// Método para la serialización de la excepción mediante ISerializable
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		/// <remarks></remarks>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule")]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			//Aqui se deben agregar los datos específicos de esta excepción, por ejemplo:
			//info.AddValue("PropiedadX", Me.PropiedadX, GetType(String))
			//info.AddValue("PropiedadY", Me.PropiedadY, GetType(List(Of Integer)))       
		}



		//////////////



	}
}
