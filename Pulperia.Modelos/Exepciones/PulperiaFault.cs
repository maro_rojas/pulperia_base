﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Pulperia.Modelos.Excepciones 
{
	

		[DataContract(Namespace = "http://Pulperia.Servicios.Modelos")]
		public class PulperiaFault 
		{

			public const string Pulperia_Faults_Action = "http://servicios/Pulperia/faults";
			public PulperiaFault()
			{
				this.Codigo = 0;
				this.Mensaje = "Ocurrió un error en el servicio";
				this.IdManejo = Guid.Empty;
			}

			[DataMember()]
			public Guid IdManejo { get; set; }

			[DataMember()]
			public int Codigo { get; set; }

			[DataMember()]
			public string Mensaje { get; set; }

		}
	
}
