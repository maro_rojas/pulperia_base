﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pulperia.Pruebas.Comunes.Ayudantes;
using Pulperia.PruebasFuncionales.Contenedores;
using Pulperia.Pruebas.Comunes.Extensiones;
using Comunes.Extensiones;

namespace Pulperia.PruebasFuncionales.Ayudantes
{
    public class AyudantePruebaAgregarArticulo : AyudanteBase<EscenarioArticulo>
    {
        public override List<EscenarioArticulo> CargarEscenarios()
        {
            return new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioArticulo>(Recursos.Recursos.Articulos);
        }

        public override void InicializarPrueba(List<EscenarioArticulo> listaEscenarios)
        {
            var todos = (from escenario in listaEscenarios select escenario.CodArticulo).ToList();

            var existentes = listaEscenarios.Where(e => e.CodEstado == 2 || e.CodEstado == 1).ToList();

            var inactivos = listaEscenarios.Where(e => e.CodEstado == 1).ToList();

            //Paso 1.1: Eliminar los que no existen
            EliminarTodos(todos);

            ////Paso 1.2: Agregar los existentes
            AgregarEscenarios(existentes);

            ////Paso 1.3: Inactivar los Inactivos
            InactivarEscenarios(inactivos);
        }

        private void InactivarEscenarios(List<EscenarioArticulo> inactivos)
        {
            foreach (var item in inactivos)
            {
                var servicio = new Servicios.Articulo.ArticuloClient();
                try
                {
                    Action<EscenarioArticulo, Modelos.Articulo> customAction = (Source, Destination) => { };

                    var articulo = Mapper.MapObjects<EscenarioArticulo, Modelos.Articulo>(item, customAction);

                    servicio.InactivarArticulo(articulo);
                }
                finally
                {
                    servicio.Dispose();
                }
            }
        }

        private void AgregarEscenarios(List<EscenarioArticulo> existentes)
        {
            foreach (var item in existentes)
            {
                //servicio Pulperia.Utilitario con SP
                //var servicio = new Utilitario.Articulo.ArticuloClient();

                //servicio Pulperia.Servicios con SP
                var servicio = new Servicios.Articulo.ArticuloClient();
                try
                {
                    Action<EscenarioArticulo, Modelos.Articulo> customAction = (Source, Destination) =>
                    {
                        //Destination.CodArticulo = Source.CodArticulo;
                        //Destination.Nombre = Source.Nombre;
                        //Destination.Precio = Source.Precio;
                    };

                    var articulo = Mapper.MapObjects<EscenarioArticulo, Modelos.Articulo>(item, customAction);

                    //servicio.InsertarExistentes(articulo);
                    servicio.InsertarArticulo(articulo);
                }
                finally
                {
                    servicio.Dispose();
                }
            }
        }

        private void EliminarTodos(List<string> todos)
        {
            foreach (var item in todos)
            {
                var servicio = new Utilitario.Articulo.UtilitarioClient();
                try
                {
                    servicio.EliminarArticulos(item);
                }
                finally
                {
                    servicio.Dispose();
                }
            }
        }

        public override void ProcesarEscenarios(IEnumerable<EscenarioArticulo> listaEscenarios)
        {

            foreach (var item in listaEscenarios)
            {
                var servicio = new Servicios.Articulo.ArticuloClient();

                Action<EscenarioArticulo, Modelos.Articulo> customAction = (Source, Destination) => { };
                var articulo = Mapper.MapObjects<EscenarioArticulo, Modelos.Articulo>(item, customAction);
                try
                {
                    servicio.InsertarArticulo(articulo);
                }
                catch (System.ServiceModel.FaultException<Modelos.Excepciones.PulperiaFault> pulperiaFault)
                {
                    item.MensajeErrorRecibido = pulperiaFault.Detail.Mensaje;
                }
                finally
                {
                    servicio.Dispose();
                }
            }
        }

        internal string ValidarEscenariosSuscripcion(List<EscenarioArticulo> listaEscenarios)
        {
            var verificador = new Pulperia.PruebasFuncionales.Verificadores.VerificadorArticulo();

            return verificador.VerificarEscenarios(listaEscenarios);
        }
    }
}
