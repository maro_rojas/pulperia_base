﻿using Pulperia.Pruebas.Comunes.Contenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.PruebasFuncionales.Contenedores
{
	public class EscenarioArticulo : EscenarioBase
	{
        public string CodArticulo { get; set; }

		public string Nombre { get; set; }

		public int Precio { get; set; }

		public int Existencia { get; set; }

		public int CodEstado { get; set; }

		public override string Resultado { get; set; }
		public override string MensajeErrorRecibido { get; set; }
		public override string MensajeErrorEsperado { get; set; }

		public Modelos.Articulo Articulo { get; set; }

		public override string ToString()
		{
			return string.Format("codArticulo: {0} - Articulo: {1} - Precio {2}", CodArticulo, Nombre, Precio);
		}

	}
}
