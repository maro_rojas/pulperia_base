﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pulperia.Modelos.Excepciones;
using Pulperia.PruebasFuncionales.Ayudantes;
using Pulperia.PruebasFuncionales.Contenedores;
using Pulperia.PruebasFuncionales.Servicios.Articulo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Pulperia.PruebasFuncionales
{
    [TestClass]
	public class PruebaAgregarArticulo
    {
		[ClassInitialize()]
		public static void Inicializar(TestContext context)
		{

		}

		[TestInitialize()]
		public void InicializarPruebas()
		{

		}

		[TestMethod]
		public void ServicioDisponible_Pruebas()
		{
			var resultado = false;
			var servicio = new Utilitario.Articulo.UtilitarioClient();
			try
			{
				resultado = servicio.ServicioDisponible();

			}
			finally
			{
				servicio.Close();
			}
			Assert.IsTrue(resultado);
		}

		[TestMethod]
		public void ListaArticulos_Pruebas()
		{
			var servicio = new ArticuloClient();
			List<Pulperia.Modelos.Articulo> listaArticulos = null;
			try
			{
				listaArticulos = servicio.ListarArticulos();

			}
			finally
			{
				servicio.Close();
			}
			Assert.IsNotNull(listaArticulos);
			Assert.IsTrue(listaArticulos.Count > 0);

		}

		[TestMethod]
		public void ListaArticulosBD_Pruebas()
		{
			var servicio = new ArticuloClient();
			List<Pulperia.Modelos.Articulo> listaArticulos = null;
			try
			{
				listaArticulos = servicio.ListarDesdeBaseDatos();
			}
			finally
			{
				servicio.Close();
			}
			Assert.IsNotNull(listaArticulos);
			Assert.IsTrue(listaArticulos.Count > 0);
		}

		[TestMethod]
		public void ObtenerPorId_Pruebas()
		{
			var servicio = new ArticuloClient();
			Pulperia.Modelos.Articulo articulo = null;
			try
			{
				articulo = servicio.ObtenerPorCodArticulo("26");
			}
			finally
			{
				servicio.Close();
			}
			Assert.IsNotNull(articulo);
			Assert.AreEqual("Arroz", articulo.Nombre);
		}

		[TestMethod]
		public void InsertarArticulo_Pruebas()
		{
			var servicio = new ArticuloClient();
			//Pulperia.Modelos.Articulo articulo = null;
			var articulo = new Modelos.Articulo()
			{
				CodArticulo = "asd123",
				Nombre = "prueba",
				Existencia = 10,
				CodEstado = 2,
				Precio = 100
			};
			try
			{
				servicio.InsertarArticulo(articulo);
			}
			finally
			{
				servicio.Close();
			}
			Assert.IsNotNull(articulo);
			Assert.AreEqual("prueba", articulo.Nombre);
		}

		[TestMethod]
		public void ActualizarArticulo_Pruebas()
		{
			var servicio = new ArticuloClient();
			//Pulperia.Modelos.Articulo articulo = null;
			var articulo = new Modelos.Articulo()
			{
				CodArticulo = "bnm123",
				Nombre = "algo",
				Existencia = 10,
				CodEstado = 1,
				Precio = 100
			};
			Modelos.Articulo a = null;
			try
			{
				servicio.ActualizarArticulo(articulo);
				a = servicio.ObtenerPorCodArticulo(articulo.CodArticulo);
			}
			finally
			{
				servicio.Close();
			}
			Assert.IsNotNull(a);
			Assert.AreEqual("algo", a.Nombre);
		}

		[TestMethod()]
		[TestCategory("Funcional")]
		[TestCategory("SinpeMovil")]
		[TestCategory("Logica Negocio")]
		[TestCategory("Suscripcion")]
        public void AgregarArticulo_Prueba()
        {
            var resultadoEsperado = "";
            var resultadoObtenido = "El resultado Obtenido debe ser igual al esperado";
			var ayudantePruebaAgregarArticulo = new AyudantePruebaAgregarArticulo();

            //Paso 1: Cargar escenarios
			List<EscenarioArticulo> listaEscenarios = ayudantePruebaAgregarArticulo.CargarEscenarios();

			//Paso 2: Preparar ambiente local
			ayudantePruebaAgregarArticulo.InicializarPrueba(listaEscenarios);

            //Paso 3: Procesar los escenarios de la prueba
			ayudantePruebaAgregarArticulo.ProcesarEscenarios(listaEscenarios);

            //Paso 4: Verificar el resultado de los escenarios
			resultadoObtenido = ayudantePruebaAgregarArticulo.ValidarEscenariosSuscripcion(listaEscenarios);

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);

        }

		[TestMethod]
		public void LanzarExcepcion_Prueba()
		{
			var errorEsperado = "Error codigo";
			var errorObtenido = "";
			var servicio = new ArticuloClient();
			try
			{
				servicio.LanzarExcepcion();
			}
			catch (FaultException<PulperiaFault> ex)
			{
				errorObtenido = ex.Detail.Mensaje;
			}
			finally
			{
				servicio.Close();
			}
			Assert.AreEqual(errorEsperado, errorObtenido);
		}


        [TestCleanup()]
        public void Finalizar()
        {

        }

    }
}
