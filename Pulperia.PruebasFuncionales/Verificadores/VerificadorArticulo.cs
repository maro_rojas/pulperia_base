﻿using Pulperia.Pruebas.Comunes.Verificadores;
using Pulperia.PruebasFuncionales.Contenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comunes.Extensiones;

namespace Pulperia.PruebasFuncionales.Verificadores
{
	class VerificadorArticulo : VerificadorBase<EscenarioArticulo>
	{

		protected override string EncabezadoEscenario(EscenarioArticulo escenario)
		{
			return string.Format("El escenario articulo con código [{0}] falló", escenario.CodArticulo);
		}

		protected override void CompletarEscenario(EscenarioArticulo escenario)
		{
			var servicioUtilitario = new Utilitario.Articulo.UtilitarioClient();

			try
			{
				//escenario.Articulo = servicioUtilitario.ObtenerPorCodArticulo(escenario.CodArticulo);
			}
			finally
			{
				servicioUtilitario.Dispose();
            }

		}

		public override string VerificarEscenarios(List<EscenarioArticulo> escenarios)
		{
			//TODO:Iterar la coleccion para verificar por cada escenario si existe o si el mensaje es igual
			resultadoValidaciones = string.Empty;

			foreach (var escenario in escenarios)
			{
				resultadoEscenario = string.Empty;
				CompletarEscenario(escenario);

				if (escenario.Resultado.ToUpper() == "EXITOSO")
					VerificarEscenarioExitoso(escenario);
				else if (escenario.Resultado.ToUpper() == "NO EXITOSO")
					VerificarEscenarioNoExitoso(escenario);

				AgregarResultadoGeneral(resultadoEscenario);
			}

			return resultadoValidaciones;

		}

		protected override void VerificarEscenarioExitoso(EscenarioArticulo escenario)
		{
			//if (escenario.CodArticulo == null)
			//{
			//	AgregarErrorEscenario(string.Format("No existe un articulo registrado ese Código [{0}].", escenario.CodArticulo));
			//}
			//else if (escenario.Articulo.CodArticulo != escenario.CodArticulo || escenario.Articulo.IdArticulo == 0)
			//	AgregarErrorEscenario(string.Format("Los datos del articulo con identificación [{0}] no son consistentes con los datos del escenario", escenario.CodArticulo));

			ReportarResultadoEscenario(EncabezadoEscenario(escenario));

		}

		protected override void VerificarEscenarioNoExitoso(EscenarioArticulo escenario)
		{
			if (escenario.Articulo != null)
			{
				//if (escenario.CodEstado != escenario.Articulo.CodEstado)
				//	AgregarErrorEscenario("El articulo existe y no debería");
			}

			//if (escenario.MensajeErrorEsperado != escenario.MensajeErrorRecibido)
			//	AgregarErrorEscenario(string.Format("El escenario con el código {0} esperaba como respuesta {1} y retornó {2}", escenario.CodArticulo, escenario.MensajeErrorEsperado, escenario.MensajeErrorRecibido));

			ReportarResultadoEscenario(EncabezadoEscenario(escenario));

		}
	}
}
