﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Pulperia.Utilitarios.Contratos;
using Pulperia.Modelos;
using System.Data.SqlClient;
using Pulperia.LogicaDeNegocio.Dominio.Caracteristicas;

namespace Pulperia.Utilitarios
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
	public class Utilitario : IUtilitario
	{
		[OperationBehavior]
		public bool ServicioDisponible()
		{
			return true;
		}
		public void EliminarArticulos(string codigoArticulo)
		{
			var parametro = new SqlParameter
			{
				ParameterName = "CodArticulo",
				Value = codigoArticulo
			};
			using (var contexto = new ContextoDatos())
			{
				try
				{
					contexto.Database.ExecuteSqlCommand("exec PR_Pulperia_BorrarArticulo @CodArticulo", parametro);
				}
				catch (Exception)
				{
					
					throw;
				}
				
			}
		}


		public void InsertarExistentes(Modelos.Articulo articulo)
		{
			var parametro1 = new SqlParameter
			{
				ParameterName = "codArticulo",
				Value = articulo.CodArticulo
			};
			var parametro2 = new SqlParameter
			{
				ParameterName = "nombreArticulo",
				Value = articulo.Nombre
			};
			var parametro3 = new SqlParameter
			{
				ParameterName = "precioArticulo",
				Value = articulo.Precio
			};
			var parametro4 = new SqlParameter
			{
				ParameterName = "existenciaArticulo",
				Value = articulo.Existencia
			};
			var parametro5 = new SqlParameter
			{
				ParameterName = "codEstado",
				Value = articulo.CodEstado
			};

			using (var contexto = new ContextoDatos())
			{
				contexto.Database.ExecuteSqlCommand("exec PR_Pulperia_InsertarExistentes @codArticulo,@nombreArticulo,@precioArticulo,@existenciaArticulo,@codEstado",
					parametro1, parametro2, parametro3, parametro4, parametro5);
			}
		}

		[OperationBehavior]
		public void InactivarInactivos(Pulperia.Modelos.Articulo articulo)
		{
            var caracteristica = new CaracteristicaAgregarArticulo();
            caracteristica.Agregar(articulo);
        }


		public Articulo ObtenerPorCodArticulo(string CodArticulo)
		{
			using (var rep = new AccesoADatos.Repositorios.Articulos())
			{
				var art = rep.Articulo.FirstOrDefault(x => x.CodArticulo == CodArticulo);
				return art;
			}
		}
	}
}
